﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Shop_Repository
{
    public class ShopRepository
    {
        
        public IEnumerable<Inventory> GetInventories()
        {
            using (shopEntities context = new shopEntities())
            {
                return context.Inventory.Include(i => i.Product).ToArray();
            }
        }

        public IEnumerable<Product> GetProducts()
        {
            using (shopEntities context = new shopEntities())
            {
                return context.Products.ToArray();
            }
        }

        public void Update(Product product)//TODO
        {
            using (shopEntities context = new shopEntities())
            {
                context.Entry(product).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void AddProduct(Product p)
        {
            using (shopEntities context = new shopEntities())
            {
                p.Code = p.Code.ToLower();
                p.Name = p.Name.ToLower();
                if (p.Color != null)
                    p.Color = p.Color.ToLower();

                context.Products.Add(p);
                context.SaveChanges();
            }
        }

        public void RemoveProduct(string code)
        {
            using (shopEntities context = new shopEntities())
            {
                if (context.Products.Any(p => p.Code == code))
                    context.Products.Remove(context.Products.First(p => p.Code == code));
                context.SaveChanges();
            }
        }

        public void RemoveInventory(string code, string size)
        {
            using (shopEntities context = new shopEntities())
            {
                var inv = context.Inventory.First(i => i.Product_Code == code && i.Size == (size == null ? "" : size.ToUpper()));
                context.Inventory.Remove(inv);
                context.SaveChanges();
            }
        }

        public void Purchase(string product_code, string size, int quantity)
        {
            using (shopEntities context = new shopEntities())
            {
                if (context.Inventory.Any(p => p.Product_Code == product_code.ToLower() && p.Size == (size == null ? "" : size.ToUpper())))
                    context.Inventory.First(p => p.Product_Code == product_code.ToLower() && p.Size == (size == null ? "" : size.ToUpper())).Quantity += quantity;
                else
                {
                    Inventory inventory = new Inventory()
                    {
                        Product_Code = product_code.ToLower(),
                        Size = size == null ? "" : size.ToUpper(),
                        Quantity = quantity,
                    };
                    context.Inventory.Add(inventory);
                }
                context.SaveChanges();
            }

        }

        public void Sell(string product_code, string size, int quantity)
        {
            using (shopEntities context = new shopEntities())
            {
                if (context.Inventory.Any(p => p.Product_Code == product_code && p.Size == (size == null ? "" : size.ToUpper())))
                {
                    if (context.Inventory.First(p => p.Product_Code == product_code && p.Size == (size == null ? "" : size.ToUpper())).Quantity >= quantity)
                    {
                        context.Inventory.First(p => p.Product_Code == product_code && p.Size == (size == null ? "" : size.ToUpper())).Quantity -= quantity;
                        context.SaveChanges();
                    }
                }
            }

        }

    }
}
